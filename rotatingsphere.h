#ifndef ROTATINGSPHERE_H
#define ROTATINGSPHERE_H

#include "robotics.hpp"

// gmlib
#include <parametrics/surfaces/gmpsphere.h>

class RotatingSphere : public GMlib::PSphere<float> {

public:
  using PSphere::PSphere;

  //SHOULDER PARAMS
  bool                shoulder_state = false;
  bool                shoulder_multipleFlag = false;
  bool                shoulder_angleUsed = false;
  int                 shoulder_neckJointAngle = 90;
  int                 shoulder_angle = 0;
  double              shoulder_count = 0.0;
  double              shoulder_diff = 0.0;
  float               shoulder_velocity = 0.001f;
  GMlib::Angle        shoulder_currentAngle = GMlib::Angle(90);
  double              shoulder_currentAngleDegree = 90;
  std::vector<double> shoulder_angles;

  //ELBOW PARAMS
  bool                elbow_state = false;
  bool                elbow_multipleFlag = false;
  bool                elbow_angleUsed = false;
  int                 elbow_neckJointAngle = 0;
  int                 elbow_angle = 0;
  double              elbow_count = 0.0;
  double              elbow_diff = 0.0;
  float               elbow_velocity = 0.001f;
  GMlib::Angle        elbow_currentAngle = GMlib::Angle(0);
  double              elbow_currentAngleDegree = 0;
  std::vector<double> elbow_angles;

  //WRIST PARAMS
  bool                wrist_state = false;
  bool                wrist_multipleFlag = false;
  bool                wrist_angleUsed = false;
  int                 wrist_neckJointAngle = 90;
  int                 wrist_angle = 0;
  double              wrist_count = 0.0;
  double              wrist_diff = 0.0;
  float               wrist_velocity = 0.001f;
  GMlib::Angle        wrist_currentAngle = GMlib::Angle(90);
  double              wrist_currentAngleDegree = 90;
  std::vector<double> wrist_angles;

  ~RotatingSphere() override {

    if(m_test03)
      remove(test_03_sphere.get());
  }

  void test03() {

    GMlib::Vector<float,3> d = evaluate(0.0f,0.0f,0,0)[0][0];
    test_03_sphere = std::make_shared<RotatingSphere,float>(1.5f);

    test_03_sphere->translate(d + d.getNormalized()*2.0f);
    test_03_sphere->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f) );
    test_03_sphere->toggleDefaultVisualizer();
    test_03_sphere->sample(200,200,1,1);
    insert(test_03_sphere.get());

    m_test03 = true;
  }

  void shoulder_setState(bool value)
  {
    shoulder_state = value;
  }

  void shoulder_setAngle(float value)
  {
    shoulder_angle = value;
    robotics.passToRobot(1, 2500-(shoulder_angle*11.11111111), 0.1/shoulder_velocity);
  }

  void shoulder_setMultipleAngles(std::vector<double> value)
  {
    shoulder_angles = value;
  }

  void shoulder_setMultipleFlag(bool value)
  {
    shoulder_multipleFlag = value;
  }

  double shoulder_getCurrentAngle()
  {
    return shoulder_currentAngleDegree;
  }

  bool shoulder_getAngleUsedFlag()
  {
    return shoulder_angleUsed;
  }


  void elbow_setState(bool value)
  {
    elbow_state = value;
  }

  void elbow_setAngle(float value)
  {
    elbow_angle = value;
    robotics.passToRobot(2, 500+(elbow_angle*11.11111111), 0.1/elbow_velocity);
  }

  void elbow_setMultipleAngles(std::vector<double> value)
  {
    elbow_angles = value;
  }

  void elbow_setMultipleFlag(bool value)
  {
    elbow_multipleFlag = value;
  }

  double elbow_getCurrentAngle()
  {
    return elbow_currentAngleDegree;
  }

  bool elbow_getAngleUsedFlag()
  {
    return elbow_angleUsed;
  }


  void wrist_setState(bool value)
  {
    wrist_state = value;
  }

  void wrist_setAngle(float value)
  {
    wrist_angle = value;
    robotics.passToRobot(3, 2500-(wrist_angle*11.11111111), 0.1/wrist_velocity);
  }

  void wrist_setMultipleAngles(std::vector<double> value)
  {
    wrist_angles = value;
  }

  void wrist_setMultipleFlag(bool value)
  {
    wrist_multipleFlag = value;
  }

  double wrist_getCurrentAngle()
  {
    return wrist_currentAngleDegree;
  }

  bool wrist_getAngleUsedFlag()
  {
    return wrist_angleUsed;
  }


private:
  Robotics robotics;

  bool m_test03 {false};

  std::shared_ptr<RotatingSphere> test_03_sphere {nullptr};

  void shoulder_setCurrentAngle(int value)
  {
    shoulder_currentAngleDegree = value;
    shoulder_currentAngle = GMlib::Angle(value);
  }

  void elbow_setCurrentAngle(int value)
  {
    elbow_currentAngleDegree = value;
    elbow_currentAngle = GMlib::Angle(value);
  }

  void wrist_setCurrentAngle(int value)
  {
    wrist_currentAngleDegree = value;
    wrist_currentAngle = GMlib::Angle(value);
  }

protected:
  void localSimulate(double /*dt*/) override {

    GMlib::Vector<float,3> rotAxis{1.0f, 0.0f, 0.0f};
    if (shoulder_state != false){
      shoulder_count+=shoulder_velocity;

      if (shoulder_multipleFlag){
        int shoulder_count = 0;
        if (!shoulder_angles.empty()) {
          shoulder_angle = shoulder_angles[shoulder_count];
          robotics.passToRobot(1, 2500-(shoulder_angles[shoulder_count]*11.11111111), 0.1/shoulder_velocity);
          if (shoulder_angleUsed){
            ++shoulder_count;
            shoulder_angle = shoulder_angles[shoulder_count];
            robotics.passToRobot(1, 2500-(shoulder_angles[shoulder_count]*11.11111111), 0.1/shoulder_velocity);
            shoulder_angleUsed = false;
          }
          if (shoulder_count >= shoulder_angles.size()-1){
            std::cout << shoulder_count << std::endl;
            shoulder_multipleFlag = false;
            shoulder_angleUsed = false;
          }
        }
      }

      double shoulder_diff = shoulder_currentAngleDegree - shoulder_angle;

      if (shoulder_diff >= 0){
        GMlib::HqMatrix<float,3> shoulder_rotation{GMlib::Angle(-shoulder_count), (rotAxis)};
        if (std::abs(double(shoulder_currentAngle - GMlib::Angle(shoulder_angle))) >= 0.001) {
          _matrix = _matrix * shoulder_rotation;
          shoulder_currentAngle -= GMlib::Angle(shoulder_count);
          shoulder_count = 0;
        }
        else{
          shoulder_angleUsed = true;
          shoulder_setCurrentAngle(shoulder_angle);
        }
      }

      else if (shoulder_diff < 0){
        GMlib::HqMatrix<float,3> shoulder_rotation{GMlib::Angle(shoulder_count), (rotAxis)};
        if (std::abs(double(shoulder_currentAngle - GMlib::Angle(shoulder_angle))) >= 0.001) {
          _matrix = _matrix * shoulder_rotation;
          shoulder_currentAngle += GMlib::Angle(shoulder_count);
          shoulder_count = 0;
        }
        else{
          shoulder_angleUsed = true;
          shoulder_setCurrentAngle(shoulder_angle);
        }
      }
    }

    if (elbow_state != false){
      elbow_count+=elbow_velocity;

      if (elbow_multipleFlag){
        int elbow_count = 0;
        if (!elbow_angles.empty()) {
          elbow_angle = elbow_angles[elbow_count];
          robotics.passToRobot(2, 500+(elbow_angles[elbow_count]*11.11111111), 0.1/elbow_velocity);
          if (elbow_angleUsed){
            ++elbow_count;
            elbow_angle = elbow_angles[elbow_count];
            robotics.passToRobot(2, 500+(elbow_angles[elbow_count]*11.11111111), 0.1/elbow_velocity);
            elbow_angleUsed = false;
          }
          if (elbow_count >= elbow_angles.size()-1){
            elbow_multipleFlag = false;
            elbow_angleUsed = false;
          }
        }
      }

      double elbow_diff = elbow_currentAngleDegree - elbow_angle;

      if (elbow_diff >= 0){
        GMlib::HqMatrix<float,3> elbow_rotation{GMlib::Angle(-elbow_count), (rotAxis)};
        if (std::abs(double(elbow_currentAngle - GMlib::Angle(elbow_angle))) >= 0.001) {
          _matrix = _matrix * elbow_rotation;
          elbow_currentAngle -= GMlib::Angle(elbow_count);
          elbow_count = 0;
        }
        else{
          elbow_angleUsed = true;
          elbow_setCurrentAngle(elbow_angle);
        }
      }

      else if (elbow_diff < 0){
        GMlib::HqMatrix<float,3> elbow_rotation{GMlib::Angle(elbow_count), (rotAxis)};
        if (std::abs(double(elbow_currentAngle - GMlib::Angle(elbow_angle))) >= 0.001) {
          _matrix = _matrix * elbow_rotation;
          elbow_currentAngle += GMlib::Angle(elbow_count);
          elbow_count = 0;
        }
        else{
          elbow_angleUsed = true;
          elbow_setCurrentAngle(elbow_angle);
        }
      }
    }

    if (wrist_state != false){
      wrist_count+=wrist_velocity;

      if (wrist_multipleFlag){
        int wrist_count = 0;
        if (!wrist_angles.empty()) {
          wrist_angle = wrist_angles[wrist_count];
          robotics.passToRobot(3, 2500-(wrist_angle*11.11111111), 0.1/wrist_velocity);
          if (wrist_angleUsed){
            ++wrist_count;
            wrist_angle = wrist_angles[wrist_count];
            robotics.passToRobot(3, 2500-(wrist_angle*11.11111111), 0.1/wrist_velocity);
            wrist_angleUsed = false;
          }
          if (wrist_count >= wrist_angles.size()-1){
            std::cout << wrist_count << std::endl;
            wrist_multipleFlag = false;
            wrist_angleUsed = false;
          }
        }
      }

      double wrist_diff = wrist_currentAngleDegree - wrist_angle;

      if (wrist_diff >= 0){
        GMlib::HqMatrix<float,3> wrist_rotation{GMlib::Angle(-wrist_count), (rotAxis)};
        if (std::abs(double(wrist_currentAngle - GMlib::Angle(wrist_angle))) >= 0.001) {
          _matrix = _matrix * wrist_rotation;
          std::cout << "goint to angle " << GMlib::Angle(wrist_angle) << std::endl;
          wrist_currentAngle -= GMlib::Angle(wrist_count);
          std::cout << "current angle is " << double(wrist_currentAngle) << std::endl;
          wrist_count = 0;
        }
        else{
          wrist_angleUsed = true;
          wrist_setCurrentAngle(wrist_angle);
          std::cout << "current angle is now at " << wrist_currentAngleDegree << std::endl;
        }
      }

      else if (wrist_diff < 0){
        GMlib::HqMatrix<float,3> wrist_rotation{GMlib::Angle(wrist_count), (rotAxis)};
        if (std::abs(double(wrist_currentAngle - GMlib::Angle(wrist_angle))) >= 0.001) {
          _matrix = _matrix * wrist_rotation;
          std::cout << "goint to angle " << GMlib::Angle(wrist_angle) << std::endl;
          wrist_currentAngle += GMlib::Angle(wrist_count);
          std::cout << "current angle is " << double(wrist_currentAngle) << std::endl;
          wrist_count = 0;
        }
        else{
          wrist_angleUsed = true;
          wrist_setCurrentAngle(wrist_angle);
          std::cout << "current angle is now at " << wrist_currentAngleDegree << std::endl;
        }
      }
    }

  }

}; // END class RotatingSphere



#endif // RotatingSphere_H
