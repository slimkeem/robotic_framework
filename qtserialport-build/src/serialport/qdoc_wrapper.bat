@echo off
SetLocal EnableDelayedExpansion
(set QT_VERSION=6.0.0)
(set QT_VER=6.0)
(set QT_VERSION_TAG=600)
(set QT_INSTALL_DOCS=C:/Qt/Docs/Qt-5.13.1)
(set BUILDDIR=D:/slimk/Documents/VFX/qmldemo/qtserialport-build/src/serialport)
C:\Qt\5.13.1\msvc2017_64\bin\qdoc.exe %*
EndLocal
