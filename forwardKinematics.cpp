#include "forwardKinematics.hpp"
#include <cmath>
#include <iostream>
#include <math.h>

//#include "inverseKinematics.hpp"

//InverseKinematics _ik;

////should take as input: The Joint Angles
////return the cartesian position and orientation of end effector


//void ForwardKinematics::rotateMatrices(GMlib::Vector<float,4> newConfig)
//{
//  auto deltaConfig = _ik.getCurrentConfig() - newConfig;

//  GMlib::Vector<float,3> revolutionaryAxis = GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f);               //spatial orientation
//  GMlib::Vector<float,3> prismaticAxis     = GMlib::Vector<float,3>(1.0f, 0.0f, 0.0f);

//  auto revLIV = revolutionaryAxis.getLinIndVec();
//  auto revU = revLIV ^ revolutionaryAxis;
//  auto revV = revU ^ revolutionaryAxis;

//  auto prisLIV = prismaticAxis.getLinIndVec();
//  auto prisU = prisLIV ^ prismaticAxis;
//  auto prisV = prisU ^ prismaticAxis;

//  GMlib::HqMatrix<float,3> baseMatrix     = _ik.getBaseMatrix();
//  GMlib::HqMatrix<float,3> shoulderMatrix =   _ik.getShoulderMatrix();
//  GMlib::HqMatrix<float,3> elbowMatrix    =   _ik.getElbowMatrix();
//  GMlib::HqMatrix<float,3> wristMatrix    =   _ik.getWristMatrix();
//  GMlib::HqMatrix<float,3> handMatrix     =   _ik.getEndEffectorMatrix();

//  baseMatrix.rotate(int(deltaConfig[0]),revU,revV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  shoulderMatrix.rotate(int(deltaConfig[1]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  elbowMatrix.rotate(int(deltaConfig[2]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  wristMatrix.rotate(int(deltaConfig[3]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));

//  _ik.setBaseMatrix(baseMatrix);
//  _ik.setShoulderMatrix(shoulderMatrix);
//  _ik.setElbowMatrix(elbowMatrix);
//  _ik.setWristMatrix(wristMatrix);
//  _ik.setEndEffectorMatrix(handMatrix);

//  shoulderMatrix = baseMatrix * shoulderMatrix;
//  elbowMatrix = shoulderMatrix * armMatrix * elbowMatrix;
//  wristMatrix = elbowMatrix * foreArmMatrix * wristMatrix;
//  handMatrix = wristMatrix * handMatrix;

//  _ik.setBasePosition(GMlib::Point<float,3>(baseMatrix[0][3],baseMatrix[1][3], baseMatrix[2][3]));
//  _ik.setShoulderPosition(GMlib::Point<float,3>(shoulderMatrix[0][3],shoulderMatrix[1][3], shoulderMatrix[2][3]));
//  _ik.setElbowPosition(GMlib::Point<float,3>(elbowMatrix[0][3],elbowMatrix[1][3], elbowMatrix[2][3]));
//  _ik.setWristPosition(GMlib::Point<float,3>(wristMatrix[0][3],wristMatrix[1][3], wristMatrix[2][3]));
//  _ik.setEndEffectorPosition(GMlib::Point<float,3>(handMatrix[0][3],handMatrix[1][3], handMatrix[2][3]));
//}








void ForwardKinematics::computeForwardKinematics(GMlib::Vector<float, 4> initialConfig,
                                                 GMlib::Vector<float,4>  heights)
{
  auto denavitHartenberg = populateDenavitHartenberg(initialConfig, heights);
  GMlib::SqMatrix<float, 4> joint;

  for (int i = 0; i < 4; ++i) {
    auto zMatrix = computeTransZ(denavitHartenberg[i][0]) * computeRotZ(denavitHartenberg[i][1]);
    zMatrices.push_back(zMatrix);

    auto xMatrix = computeTransX(denavitHartenberg[i][2]) * computeRotX(denavitHartenberg[i][3]);
    xMatrices.push_back(xMatrix);

    joint = joint * zMatrices.at(i) * xMatrices.at(i);
    joints.push_back(joint);

    std::cout << joint << "\n" <<std::endl;
////    std::cout << "joints";
  }
//  joint.setCol(GMlib::Vector<float,4> (0,0,0,0),0);
//  std::cout << joint << std::endl;
}


GMlib::SqMatrix<float, 4> ForwardKinematics::populateDenavitHartenberg(GMlib::Vector<float,4> initialConfig,
                                                                       GMlib::Vector<float,4> heights)
{
  GMlib::SqMatrix<float, 4> dh;

  dh.setCol(heights,0);
  dh.setCol(GMlib::Vector<float,4> (initialConfig[0],0,0,0),1); //theta
  dh.setCol(GMlib::Vector<float,4> (0,0,0,0),2); //a
  dh.setCol(GMlib::Vector<float,4> (0,initialConfig[1],initialConfig[2],initialConfig[3]),3);

  return dh;
}


GMlib::SqMatrix<float, 4> ForwardKinematics::computeTransZ(double d)
{
  GMlib::SqMatrix<float, 4> Tz;
  Tz[2][3] = d;

  return Tz;
}

GMlib::SqMatrix<float, 4> ForwardKinematics::computeTransX(double d)
{
  GMlib::SqMatrix<float, 4> Tx;
  Tx[0][3] = d;

  return Tx;
}

GMlib::SqMatrix<float, 4> ForwardKinematics::computeRotZ(double d)
{
  GMlib::SqMatrix<float, 4> Rz;
  Rz[0][0], Rz[1][1] = std::cos(d);
  Rz[0][1] = -std::sin(d);
  Rz[1][0] = std::sin(d);

  return Rz;
}

GMlib::SqMatrix<float, 4> ForwardKinematics::computeRotX(double d)
{
  GMlib::SqMatrix<float, 4> Rx;
  Rx[1][1], Rx[2][2] = std::cos(d);
  Rx[1][2] = -std::sin(d);
  Rx[2][1] = std::sin(d);

  return Rx;
}

GMlib::HqMatrix<float, 4> ForwardKinematics::getJointMatrix() const
{
  return jointMatrix;
}

void ForwardKinematics::setJointMatrix(const GMlib::HqMatrix<float, 4> &value)
{
  jointMatrix = value;
}

















//  GMlib::HqMatrix<float,3> baseMatrix = base_neck->getMatrix();
//  baseMatrix.rotate(int(deltaConfig[0]),revU,revV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  _ik.setBaseMatrix(baseMatrix);

//    _ik.JacobianIK(initialConfig, base_neck, shoulder, elbow, wrist, hand);
//  GMlib::HqMatrix<float,3> shoulderMatrix = baseMatrix * shoulder->getMatrix();
//  shoulderMatrix.rotate(int(deltaConfig[1]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  _ik.setShoulderMatrix(shoulderMatrix);

//  GMlib::HqMatrix<float,3> elbowMatrix = shoulderMatrix * arm->getMatrix()* elbow->getMatrix();
//  elbowMatrix.rotate(int(deltaConfig[2]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  _ik.setElbowMatrix(elbowMatrix);

//  moveNeck(_ik.getFinalConfig()[0]);
//  moveShoulder(_ik.getFinalConfig()[1]);
//  moveElbow(_ik.getFinalConfig()[2]);
//  moveWrist(_ik.getFinalConfig()[3]);

//  GMlib::HqMatrix<float,3> wristMatrix = elbowMatrix * fore_arm->getMatrix() * wrist->getMatrix();
//  wristMatrix.rotate(int(deltaConfig[3]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
//  _ik.setWristMatrix(wristMatrix);

//  auto handMatrix = wristMatrix * hand->getMatrix();
//  _ik.setEndEffectorMatrix(handMatrix);

////  auto bP = GMlib::Point<float, 3>(bM[0][3],bM[1][3],bM[2][3]/*, bM[3][3]*/);
////  auto sP = GMlib::Point<float, 3>(sM[0][3],sM[1][3],sM[2][3]/*, sM[3][3]*/);
////  auto eP = GMlib::Point<float, 3>(eM[0][3],eM[1][3],eM[2][3]/*, eM[3][3]*/);
////  auto wP = GMlib::Point<float, 3>(wM[0][3],wM[1][3],wM[2][3]/*, wM[3][3]*/);
////  auto endP = GMlib::Point<float, 3>(endM[0][3],endM[1][3],endM[2][3]/*,endM[3][3]*/);

//  std::cout << "\n ORIGINAL \n" << hand->getMatrixGlobal() <<std::endl;

////  eM.rotate(int(deltaConfig[2]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
////  wM.rotate(int(deltaConfig[3]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));

////  bM.setCol((0.0f,0.0f,0.0f), 3);
////  bM[3][3] = 1;

////  bP =   bP;
////  sP =   bM * sP;
////  eP =   bM * sM * eP;
////  wP =   bM * sM * eM * wP;
////  endP = bM * sM * eM * wM * endP;

////  _ik.setShoulderPosition(shoulderMatrix);
////  _ik.setElbowPosition(elbowMatrix);
////  _ik.setWristPosition(wristMatrix);
////  _ik.setEndEffectorPosition(endEffectorMatrix);

//  std::cout << "\n ROTATE \n" << handMatrix <<std::endl;










































