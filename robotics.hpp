#ifndef ROBOTICS_H
#define ROBOTICS_H

#include <QSerialPort>
#include <QSerialPortInfo>

class Robotics
{

  private:
    QSerialPort    serial;

  public:
    void passToRobot(int jointValue, float positionValue, int speedValue);
};

#endif INVERSEKINEMATICS_H
