#include "robotics.hpp"


void Robotics::passToRobot(int jointValue, float positionValue, int speedValue)
{
  std::string joint = "#" + std::to_string(jointValue);
  std::string position = "P" + std::to_string(positionValue);
  std::string speed = "S" + std::to_string(speedValue);

  serial.setPortName("COM3");
  serial.setBaudRate(QSerialPort::Baud9600, QSerialPort::AllDirections);
  serial.open(QIODevice::ReadWrite);


  const QByteArray testData = QByteArray::fromStdString(joint + position + speed + "\r");

//  serial.write(testData);
}
