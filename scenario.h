#ifndef SCENARIO_H
#define SCENARIO_H

#include <QSerialPort>
#include <QSerialPortInfo>

#include <core/containers/gmarray.h>
#include <core/containers/gmdmatrix.h>
#include <scene/gmsceneobject.h>
#include "application/gmlibwrapper.h"


// qt
#include <QObject>

class RotatingCylinder;


class Scenario : public GMlibWrapper {
  Q_OBJECT
public:
  using GMlibWrapper::GMlibWrapper;

  void    initializeScenario() override;
  void    cleanupScenario() override;

  void    moveNeck(int i);
  void    moveShoulder(int i);
  void    moveElbow(int i);
  void    moveWrist(int i);




  void rotateTargetAngles();
  void testRotateTargetAngles();


  //PUBLIC GETTERS
  GMlib::SqMatrix<float, 4> getTransMat();

  GMlib::Point<float, 3> getEndEffectorPos();

  GMlib::Point<float, 3> getDesiredPos();


  //SETTERS
  void setDesiredPosition(GMlib::Point<float, 3> value);


  void setNeckJointAngle(int value);

  void setShoulderJointAngle(int value);

  void setElbowJointAngle(int value);

  void setWristJointAngle(int value);



  void getJointGlobalPos();
//  void passToRobot(int jointValue, float positionValue, int speedValue);
//  void localSimulate(double dt);

  void moveNeckMultiple(std::vector<double> angles);
  void moveShoulderMultiple(std::vector<double> angles);
  void moveElbowMultiple(std::vector<double> angles);
  void moveWristMultiple(std::vector<double> angles);

  void drawShape();
public slots:
  void    callDefferedGL();

private:
  int     neckJointAngle     = 90;
  int     shoulderJointAngle = 90;
  int     elbowJointAngle    = 0;
  int     wristJointAngle    = 90;

  bool    visibleFlag = false;

//  QSerialPort    serial;

  GMlib::Point<float, 3> desiredPos = GMlib::Point<float, 3>( 0.0f, -6.722f, -0.750795f);
  GMlib::Point<float, 3> endEffectorPos;
  //  std::shared_ptr<RotatingCylinder>       m_rotating_cylinder{nullptr};

  //PRIVATE GETTERS
  int getNeckJointAngle();
  int getShoulderJointAngle();
  int getElbowJointAngle();
  int getWristJointAngle();
  void initializeParameters();
  void rotateMatrices(GMlib::Vector<float, 4> newConfig);
  GMlib::Vector<float, 4> moveToPoint(GMlib::Point<float, 3> desiredCoordinates,
                                       GMlib::Vector<float, 4> initialConfig);
};


#endif // SCENARIO_H
