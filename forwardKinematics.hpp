#ifndef FORWARDKINEMATICS_H
#define FORWARDKINEMATICS_H


// gmlib
#include <core/containers/gmarray.h>
#include <core/containers/gmdmatrix.h>
#include <scene/gmsceneobject.h>
//#include <scene/utils/gmmaterial.h>

class RotatingCylinder;
class RotatingSphere;
class RobotCylinder;

class ForwardKinematics
{
  private:

    GMlib::SqMatrix<float,4>     populateDenavitHartenberg(GMlib::Vector<float,4> initialConfig,
                                                           GMlib::Vector<float,4> heights);

    GMlib::SqMatrix<float,4>     computeTransZ(double d);
    GMlib::SqMatrix<float,4>     computeTransX(double d);
    GMlib::SqMatrix<float,4>     computeRotZ(double d);
    GMlib::SqMatrix<float,4>     computeRotX(double d);

    std::vector<GMlib::SqMatrix<float,4>> joints;
    std::vector<GMlib::SqMatrix<float,4>> zMatrices;
    std::vector<GMlib::SqMatrix<float,4>> xMatrices;

    GMlib::HqMatrix<float,4>    jointMatrix;


  public:

    void computeForwardKinematics          (GMlib::Vector<float,4>   initialConfig,
                                            GMlib::Vector<float,4>   heights);

    GMlib::HqMatrix<float, 4> getJointMatrix() const;
    void setJointMatrix(const GMlib::HqMatrix<float, 4> &value);


//    void initializeParameters(RotatingCylinder *base_neck, RotatingSphere *shoulder, RobotCylinder *arm, RotatingSphere *elbow, RobotCylinder *fore_arm, RotatingSphere *wrist, RobotCylinder *hand);
//    void rotateMatrices(GMlib::Vector<float, 4> newConfig);
};

#endif FORWARDKINEMATICS_H
