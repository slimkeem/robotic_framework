#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H


#include "gmlibwrapper.h"
#include "../inverseKinematics.hpp"
#include "window.h"

#include "../scenario.h"
#include "../hidmanager/defaulthidmanager.h"

class GLContextSurfaceWrapper;

// gmlib
namespace GMlib {
  class Scene;
}

// qt
#include <QGuiApplication>

// stl
#include <memory>




class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  explicit GuiApplication(int& argc, char** argv);
  ~GuiApplication();

private:
  Window                                      _window;
  Scenario                                    _scenario;
  InverseKinematics                           _ik;
  DefaultHidManager                           _hidmanager;

  bool                                        flag = true;

private slots:
  virtual void                                onSceneGraphInitialized();
  virtual void                                onSceneGraphInvalidated();
  virtual void                                afterOnSceneGraphInitialized();
  void                                        moveNeckJoint(int i);
  void                                        moveShoulderJoint(int i);
  void                                        moveElbowJoint(int i);
  void                                        moveWristJoint(int i);

  int                                         getNeckJointAngle();
  void                                        getEPPoint();
  void                                        setDPPoint();
  void                                        getFinalJoints(int i, int j, int k, int l);

signals:
  void                                        signOnSceneGraphInitializedDone();


private:
  static std::unique_ptr<GuiApplication>      _instance;
public:
  static const GuiApplication&                instance();
};

#endif // GUIAPPLICATION_H
