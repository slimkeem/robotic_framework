import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.3

import "qrc:/qml/components"

import SceneGraphRendering 1.0

Item {
  id: root

  signal toggleHidBindView

  signal moveNeck(int i)
  signal getNeckAngle

  signal moveShoulder(int i)
//  signal getNeckAngle

  signal moveElbow(int i)
//  signal getNeckAngle

  signal moveWrist(int i)
//  signal getNeckAngle

  signal getEP

  signal setDP

  signal computeInvKin (int i, int j, int k, int l)


  onToggleHidBindView: hid_bind_view.toggle()

  Renderer {
    id: renderer

    anchors.fill: parent

    rcpair_name: rc_pair_cb.currentText

    ComboBox {
      id: rc_pair_cb
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.margins: 5

      width: 128

      opacity: 0.7

      model: rc_name_model
      textRole: "display"
      currentIndex: 0
    }

    Button {
      text: "?"
      anchors.top: parent.top
      anchors.right: parent.right
      anchors.margins: 5

      width: height
      opacity: 0.7

      onClicked: hid_bind_view.toggle()
    }

    Button {
      id: getEPButton
      text: "get endEffectorPos"
      anchors.top: parent.top
//      anchors.right: parent.right
      anchors.margins: 50
      anchors.left: parent.left

      width: 128
      opacity: 0.7

      onClicked: getEP()
    }

    Button {
      id: setDPButton
      text: "set desiredPos"
      anchors.top: getEPButton.top
//      anchors.right: parent.right
      anchors.margins: 50
      anchors.left: parent.left

      width: 128
      opacity: 0.7

      onClicked: setDP()
    }

    Button {
      id: computeIK
      text: "compute"
      anchors.top: setDPButton.top
//      anchors.right: parent.right
      anchors.margins: 50
      anchors.left: parent.left

      width: 128
      opacity: 0.7

      onClicked: computeInvKin(baseneck.value,shoulder.value,elbow.value,wrist.value)
    }


    Row{
        id:verticalsliders
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        Slider{
            id: elbow
            from: 0
            to: 180
            value: 0 //make it a variable that receives stuff from robot
            orientation: Qt.Vertical
            anchors.bottom: verticalsliders.bottom
            anchors.margins: 20
            onMoved: moveElbow(elbow.value)
        }

        Slider{
            id: shoulder
            from: 0
            to: 180
            value: 90 //make it a variable that receives stuff from robot
            orientation: Qt.Vertical
            anchors.bottom: verticalsliders.bottom
            anchors.left: elbow.right
            anchors.margins: 20
            onMoved: moveShoulder(shoulder.value)
        }

        Slider{
            id: wrist
            from: 0
            to: 180
            value: 90 //make it a variable that receives stuff from robot
            orientation: Qt.Vertical
            anchors.bottom: verticalsliders.bottom
            anchors.right: elbow.left
            anchors.margins: 20
            onMoved: moveWrist(wrist.value)
        }

    }

    Slider{

        id: baseneck
        from: 0
        to: 180
        value: 90//Number(getNeckAngle).valueOf()
//        position: getNeckAngle
//        valueAt:getNeckAngle()
        orientation: Qt.Horizontal
        anchors.top: verticalsliders.top
        anchors.horizontalCenter: verticalsliders.horizontalCenter
        onMoved: moveNeck(baseneck.value)
//        onValueChanged: moveNeck(baseneck.value)

        function getJointAngles() {
            getNeckAngle();
        }
    }

    HidBindingView {
      id: hid_bind_view
      anchors.fill: parent
      anchors.margins: 50
      visible:false

      states: [
        State{
          name: "show"
          PropertyChanges {
            target: hid_bind_view
            visible: true
          }
        }
      ]

      function toggle() {

        if(state === "") state = "show"
        else state = ""

      }




    }
  }

}

