#include "guiapplication.h"

// local
#include "window.h"
#include "gmlibwrapper.h"

// hidmanager
#include "../hidmanager/defaulthidmanager.h"
#include "../hidmanager/hidmanagertreemodel.h"

// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QStringListModel>
#include <QOpenGLContext>

// stl
#include <cassert>


std::unique_ptr<GuiApplication> GuiApplication::_instance {nullptr};


GuiApplication::GuiApplication(int& argc, char **argv) : QGuiApplication(argc, argv) {

  assert(!_instance);
  _instance = std::unique_ptr<GuiApplication>(this);


  qRegisterMetaType<HidInputEvent::HidInputParams> ("HidInputEvent::HidInputParams");

  connect( &_window, &Window::sceneGraphInitialized,
           this,     &GuiApplication::onSceneGraphInitialized,
           Qt::DirectConnection );

  connect( &_window, &Window::sceneGraphInvalidated,
           this,     &GuiApplication::onSceneGraphInvalidated,
           Qt::DirectConnection );

  connect( this, &GuiApplication::signOnSceneGraphInitializedDone,
           this, &GuiApplication::afterOnSceneGraphInitialized );

  connect( this, &QGuiApplication::lastWindowClosed,
           this, &QGuiApplication::quit );

  _window.rootContext()->setContextProperty( "rc_name_model", &_scenario.rcNameModel() );
  _window.rootContext()->setContextProperty( "hidmanager_model", _hidmanager.getModel() );
  _window.setSource(QUrl("qrc:///qml/main.qml"));

  _window.show();
}

GuiApplication::~GuiApplication() {

  _scenario.stop();
  _window.setPersistentOpenGLContext(false);
  _window.setPersistentSceneGraph(false);
  _window.releaseResources();
  _instance.release();

  qDebug() << "Bye bye ^^, ~~ \"emerge --oneshot life\"";
}

void
GuiApplication::onSceneGraphInitialized() {

  qDebug() << "GL context: " << QOpenGLContext::currentContext()->format();

  // Init GMlibWrapper
  _scenario.initialize();
  _hidmanager.init(_scenario);
  connect( &_scenario,  &GMlibWrapper::signFrameReady,
           &_window,    &Window::update );

  // Init test scene of the GMlib wrapper
  _scenario.initializeScenario();
  _scenario.prepare();

  emit signOnSceneGraphInitializedDone();
}

void GuiApplication::onSceneGraphInvalidated() {

  _scenario.cleanUp();
}

void
GuiApplication::afterOnSceneGraphInitialized() {

  // Hidmanager setup
  _hidmanager.setupDefaultHidBindings();
  connect( &_window, &Window::signKeyPressed,         &_hidmanager, &StandardHidManager::registerKeyPressEvent );
  connect( &_window, &Window::signKeyReleased,        &_hidmanager, &StandardHidManager::registerKeyReleaseEvent );
  connect( &_window, &Window::signMouseDoubleClicked, &_hidmanager, &StandardHidManager::registerMouseDoubleClickEvent);
  connect( &_window, &Window::signMouseMoved,         &_hidmanager, &StandardHidManager::registerMouseMoveEvent );
  connect( &_window, &Window::signMousePressed,       &_hidmanager, &StandardHidManager::registerMousePressEvent );
  connect( &_window, &Window::signMouseReleased,      &_hidmanager, &StandardHidManager::registerMouseReleaseEvent );
  connect( &_window, &Window::signWheelEventOccurred, &_hidmanager, &StandardHidManager::registerWheelEvent );

  // Handle HID OpenGL actions; needs to have the OGL context bound;
  // QQuickWindow's beforeRendering singnal provides that on a DirectConnection
  connect( &_window, &Window::beforeRendering,        &_hidmanager, &DefaultHidManager::triggerOGLActions,
           Qt::DirectConnection );

  // Register an application close event in the hidmanager;
  // the QWindow must be closed instead of the application being quitted,
  // this is to make sure that GL exits gracefully
  QString ha_id_var_close_app =
  _hidmanager.registerHidAction( "Application", "Quit", "Close application!", &_window, SLOT(close()));
  _hidmanager.registerHidMapping( ha_id_var_close_app, new KeyPressInput( Qt::Key_Q, Qt::ControlModifier) );

  // Connect some application spesific inputs.
  connect( &_hidmanager, &DefaultHidManager::signToggleSimulation,
           &_scenario,   &GMlibWrapper::toggleSimulation );

  connect( &_hidmanager,          SIGNAL(signOpenCloseHidHelp()),
           _window.rootObject(),  SIGNAL(toggleHidBindView()) );

  // Update RCPair name model
  _scenario.updateRCPairNameModel();

  // Start simulator
  _scenario.start();


  connect( &_window, &Window::beforeRendering, &_scenario, &Scenario::callDefferedGL, Qt::DirectConnection );

  connect( _window.rootObject(),
           SIGNAL(moveNeck(int)),
           this,
           SLOT(moveNeckJoint(int)));

  connect( _window.rootObject(),
           SIGNAL(getNeckAngle()),
           this,
           SLOT(getNeckJointAngle()));

  connect( _window.rootObject(),
           SIGNAL(moveShoulder(int)),
           this,
           SLOT(moveShoulderJoint(int)));

  connect( _window.rootObject(),
           SIGNAL(moveElbow(int)),
           this,
           SLOT(moveElbowJoint(int)));

  connect( _window.rootObject(),
           SIGNAL(moveWrist(int)),
           this,
           SLOT(moveWristJoint(int)));

  connect( _window.rootObject(),
           SIGNAL(getEP()),
           this,
           SLOT(getEPPoint()));

  connect( _window.rootObject(),
           SIGNAL(setDP()),
           this,
           SLOT(setDPPoint()));

  connect( _window.rootObject(),
           SIGNAL(computeInvKin(int,int,int,int)),
           this,
           SLOT(getFinalJoints(int,int,int,int)));
}

const GuiApplication& GuiApplication::instance() {  return *_instance; }

void GuiApplication::getEPPoint()
{
  std::cout << "current EP point: " << _scenario.getEndEffectorPos() <<std::endl;
//  std::cout << "current DP point: " << _scenario.getDesiredPos() << std::endl;
}

void GuiApplication::setDPPoint()
{
  _scenario.setDesiredPosition(_scenario.getEndEffectorPos());
  std::cout << "current DP point: " << _scenario.getDesiredPos() << std::endl;
}

void GuiApplication::getFinalJoints(int i, int j, int k, int l)
{
//    _scenario.testRotateTargetAngles();
//    _scenario.rotateTargetAngles();
    _scenario.drawShape();
}

int GuiApplication::getNeckJointAngle()
{
  _scenario.getJointGlobalPos();
  std::cout<< "YESSSSSSSSSSSSSSSS!!!" << std::endl;
  return 1;
}

void GuiApplication::moveNeckJoint(int i)
{
  _scenario.moveNeck(i);
}

void GuiApplication::moveShoulderJoint(int i)
{
  _scenario.moveShoulder(i);
}

void GuiApplication::moveElbowJoint(int i)
{
  _scenario.moveElbow(i);
}

void GuiApplication::moveWristJoint(int i)
{
  _scenario.moveWrist(i);
}

