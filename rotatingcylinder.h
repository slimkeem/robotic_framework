#ifndef ROTATINGCYLINDER_H
#define ROTATINGCYLINDER_H

#include "robotics.hpp"

// gmlib
#include <parametrics/surfaces/gmpcylinder.h>

class RotatingCylinder : public GMlib::PCylinder<float> {

public:
  using PCylinder::PCylinder;

  //BASE PARAMS
  bool                state = false;
  bool                multipleFlag = false;
  bool                angleUsed = false;
  int                 neckJointAngle = 90;
  int                 angle = 0;
  double              count = 0.0;
  double              diff = 0.0;
  float               velocity = 0.001f;
  GMlib::Angle        currentAngle = GMlib::Angle(90);
  double              currentAngleDegree = 90;
  std::vector<double> angles;

  ~RotatingCylinder() override {

    if(m_test02)
      remove(test_02_cylinder.get());
  }

  void test02() {

    GMlib::Vector<float,3> d = evaluate(0.0f,0.0f,0,0)[0][0];
    test_02_cylinder = std::make_shared<RotatingCylinder,float,float,float>(1.5f,0.5f,0.5f);

    test_02_cylinder->translate(d + d.getNormalized()*2.0f);
    test_02_cylinder->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f) );
    test_02_cylinder->toggleDefaultVisualizer();
    test_02_cylinder->sample(200,200,1,1);
    insert(test_02_cylinder.get());

    m_test02 = true;
  }

  void setState(bool value)
  {
    state = value;
  }

  void setAngle(float value)
  {
    angle = value;
    robotics.passToRobot(0, 2500-(angle*11.11111111), 0.1/velocity);
  }

  void setMultipleAngles(std::vector<double> value)
  {
    angles = value;
  }

  void setMultipleFlag(bool value)
  {
    multipleFlag = value;
  }

  double getCurrentAngle()
  {
    return currentAngleDegree;
  }

  bool getAngleUsedFlag()
  {
    return angleUsed;
  }

private:
  Robotics robotics;

  bool m_test02 {false};

  std::shared_ptr<RotatingCylinder> test_02_cylinder {nullptr};

  void setCurrentAngle(int value)
  {
    currentAngleDegree = value;
    currentAngle = GMlib::Angle(value);
  }

protected:
  void localSimulate(double /*dt*/) override {

    if (state != false){
      GMlib::Vector<float,3> rotAxis{0.0f, 0.0f, 1.0f};
      count+=velocity;

      if (multipleFlag){
        int count = 0;
        if (!angles.empty()) {
          angle = angles[count];
          robotics.passToRobot(0, 2500-(angles[count]*11.11111111), 0.1/velocity);
          if (angleUsed){
            ++count;
            angle = angles[count];
            robotics.passToRobot(0, 2500-(angles[count]*11.11111111), 0.1/velocity);
            angleUsed = false;
          }
          if (count >= angles.size()-1){
//            std::cout << count << std::endl;
            multipleFlag = false;
            angleUsed = false;
          }
        }
      }

      double diff = currentAngleDegree - angle;

      if (diff >= 0){
        GMlib::HqMatrix<float,3> rotation{GMlib::Angle(-count), (rotAxis)};
        if (std::abs(double(currentAngle - GMlib::Angle(angle))) >= 0.001) {
          _matrix = _matrix * rotation;
          std::cout << "goint to angle " << GMlib::Angle(angle) << std::endl;
          currentAngle -= GMlib::Angle(count);
          std::cout << "current angle is " << double(currentAngle) << std::endl;
          count = 0;
        }
        else{
          angleUsed = true;
          setCurrentAngle(angle);
          std::cout << "current angle is now at " << currentAngleDegree << std::endl;
        }
      }

      else if (diff < 0){
        GMlib::HqMatrix<float,3> rotation{GMlib::Angle(count), (rotAxis)};
        if (std::abs(double(currentAngle - GMlib::Angle(angle))) >= 0.001) {
          _matrix = _matrix * rotation;
          std::cout << "goint to angle " << GMlib::Angle(angle) << std::endl;
          currentAngle += GMlib::Angle(count);
          std::cout << "current angle is " << double(currentAngle) << std::endl;
          count = 0;
        }
        else{
          angleUsed = true;
          setCurrentAngle(angle);
          std::cout << "current angle is now at " << currentAngleDegree << std::endl;
        }
      }
    }
  }
}; // END class RotatingCylinder



#endif // RotatingCylinder_H


