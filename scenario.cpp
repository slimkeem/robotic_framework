

#include <iostream>

#include "scenario.h"
#include "testtorus.h"
#include "robotcylinder.h"
#include "rotatingcylinder.h"
#include "rotatingsphere.h"

#include "inverseKinematics.hpp"
//#include "forwardKinematics.hpp"

// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>

// qt
#include <QQuickItem>


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}

auto base_neck = new RotatingCylinder(1.0f, 1.0f, 1.0f);
auto shoulder = new RotatingSphere(0.1f);
auto arm = new RobotCylinder(1.0f, 0.2f, 4.0f);
auto elbow = new RotatingSphere(0.1f);
auto fore_arm = new RobotCylinder(1.0f, 0.2f, 4.0f);
auto wrist = new RotatingSphere(0.1f);
auto hand = new RobotCylinder(1.0f, 0.2f, 1.0f);
auto pen = new RobotCylinder(0.05f, 0.2f, 2.0f);
auto pen_tip = new RotatingSphere(0.1);
//auto line = new GMlib::PathTrack();

InverseKinematics                     _ik;
//ForwardKinematics                     _fk;

void Scenario::initializeScenario() {

  //initialize params
  neckJointAngle     = getNeckJointAngle()   ;
  shoulderJointAngle = getShoulderJointAngle();
  elbowJointAngle    = getElbowJointAngle()   ;
  wristJointAngle    = getWristJointAngle()   ;

  //  moveNeck(neckJointAngle);
  //  moveShoulder(shoulderJointAngle);
  //  moveElbow(elbowJointAngle);
  //  moveWrist(wristJointAngle);

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  //  proj_rcpair.camera->rotateGlobal(GMlib::Angle(-90), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f));
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -45.0f, 45.0f ) );
  //  proj_rcpair.camera->translate( GMlib::Vector<float,3>( 0.0f, 0.0f, 90.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  /***************************************************************************
   *                                                                         *
   * Standar example, including path track and path track arrows             *
   *                                                                         *
   ***************************************************************************/

  GMlib::Material mm(GMlib::GMmaterial::jade());
  mm.set(45.0);


  base_neck->toggleDefaultVisualizer();
  base_neck->setMaterial(GMlib::GMmaterial::blackRubber());
  base_neck->sample(60,60,1,1);
  this->scene()->insert(base_neck);
  base_neck->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 0.0f), true);
  //  base_neck->rotate(GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f));

  shoulder->toggleDefaultVisualizer();
  shoulder->setMaterial(GMlib::GMmaterial::blackRubber());
  shoulder->sample(60,60,1,1);
  base_neck->insert(shoulder);
  shoulder->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 0.6f), true);

  arm->toggleDefaultVisualizer();
  arm->setMaterial(GMlib::GMmaterial::polishedSilver());
  arm->sample(60,60,1,1);
  shoulder->insert(arm);
  arm->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 2.05f), true);

  elbow->toggleDefaultVisualizer();
  elbow->setMaterial(GMlib::GMmaterial::blackRubber());
  elbow->sample(60,60,1,1);
  arm->insert(elbow);
  elbow->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 2.05f), true);

  fore_arm->toggleDefaultVisualizer();
  fore_arm->setMaterial(GMlib::GMmaterial::polishedSilver());
  fore_arm->sample(60,60,1,1);
  elbow->insert(fore_arm);
  fore_arm->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 2.05f), true);

  wrist->toggleDefaultVisualizer();
  wrist->setMaterial(GMlib::GMmaterial::blackRubber());
  wrist->sample(60,60,1,1);
  fore_arm->insert(wrist);
  wrist->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 2.05f), true);

  hand->toggleDefaultVisualizer();
  hand->setMaterial(GMlib::GMmaterial::polishedSilver());
  hand->sample(60,60,1,1);
  wrist->insert(hand);
  hand->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 0.55f), true);

  pen->toggleDefaultVisualizer();
  pen->setMaterial(GMlib::GMmaterial::polishedBronze());
  pen->rotate(GMlib::Angle(M_PI/2), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f) );
  pen->sample(60,60,1,1);
  hand->insert(pen);
  pen->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 0.05f), true);

  pen_tip->toggleDefaultVisualizer();
  pen_tip->setMaterial(GMlib::GMmaterial::blackRubber());
  pen_tip->sample(60,60,1,1);
  pen->insert(pen_tip);
  pen_tip->move(GMlib::Vector<float,3>( 0.0f, 0.0f, 1.00f), true);

  auto ptrack = new GMlib::PathTrack();
  ptrack->setLineWidth(2);
  pen_tip->insert(ptrack);
  ptrack->setVisible(visibleFlag);
//  line->setLineWidth(2);
//  pen_tip->insert(line);
//  line->move(GMlib::Vector<float,3>(0.05f, 0.05f, 2.0f), true);
//  line->setVisible(false);

  //    auto ptom = new TestTorus(1.0f, 0.4f, 0.6f);
  //  ptom->toggleDefaultVisualizer();
  //  ptom->sample(60,60,1,1);
  ////  this->scene()->insert(ptom);
  //  base_neck->insert(ptom);
  //  auto ptrack2 = new GMlib::PathTrackArrows();
  //  ptrack2->setArrowLength(2);
  //  ptom->insert(ptrack2);

  //  auto ptrack = new GMlib::PathTrack();
  //  ptrack->setLineWidth(2);
  //  ptom->insert(ptrack);
  //  auto ptrack2 = new GMlib::PathTrackArrows();
  //  ptrack2->setArrowLength(2);
  //  ptom->insert(ptrack2);

}

void Scenario::cleanupScenario() {

}

//move functions
void Scenario::moveNeck(int i){
  base_neck->setAngle(i);
  base_neck->setState(true);
  setNeckJointAngle(base_neck->getCurrentAngle());
}

void Scenario::moveNeckMultiple(std::vector<double> angles){
  base_neck->setMultipleAngles(angles);
  base_neck->setMultipleFlag(true);
  base_neck->setState(true);
  setNeckJointAngle(base_neck->getCurrentAngle());
}

void Scenario::moveShoulder(int i){
  shoulder->shoulder_setAngle(i);
  shoulder->shoulder_setState(true);
  setShoulderJointAngle(shoulder->shoulder_getCurrentAngle());
}

void Scenario::moveShoulderMultiple(std::vector<double> angles){
  shoulder->shoulder_setMultipleAngles(angles);
  shoulder->shoulder_setMultipleFlag(true);
  shoulder->shoulder_setState(true);
  setShoulderJointAngle(shoulder->shoulder_getCurrentAngle());
}

void Scenario::moveElbow(int i){
  elbow->elbow_setAngle(i);
  elbow->elbow_setState(true);
  setElbowJointAngle(elbow->elbow_getCurrentAngle());
}

void Scenario::moveElbowMultiple(std::vector<double> angles){
  elbow->elbow_setMultipleAngles(angles);
  elbow->elbow_setMultipleFlag(true);
  elbow->elbow_setState(true);
  setElbowJointAngle(elbow->elbow_getCurrentAngle());
}

void Scenario::moveWrist(int i){
  wrist->wrist_setAngle(i);
  wrist->wrist_setState(true);
  setWristJointAngle(wrist->wrist_getCurrentAngle());
}

void Scenario::moveWristMultiple(std::vector<double> angles){
  wrist->wrist_setMultipleAngles(angles);
  wrist->wrist_setMultipleFlag(true);
  wrist->wrist_setState(true);
  setWristJointAngle(wrist->wrist_getCurrentAngle());
}

//setters
void Scenario::setNeckJointAngle(int value)
{
  neckJointAngle = value;
}

void Scenario::setWristJointAngle(int value)
{
  wristJointAngle = value;
}

void Scenario::setElbowJointAngle(int value)
{
  elbowJointAngle = value;
}

void Scenario::setShoulderJointAngle(int value)
{
  shoulderJointAngle = value;
}

void Scenario::setDesiredPosition(GMlib::Point<float, 3> value)
{
  desiredPos = value;
  auto currentConfig = GMlib::Vector<float,4>(getNeckJointAngle(),
                                              getShoulderJointAngle(),
                                              getElbowJointAngle(),
                                              getWristJointAngle());

  std::cout << "CURRENT CONFIG" << currentConfig << std::endl;

  //  _ik.setTargetPosition(value);

}

void Scenario::testRotateTargetAngles()
{
  _ik.initializeParameters(base_neck,shoulder,arm,elbow,fore_arm,wrist,hand,pen,pen_tip);
  _ik.setTargetPosition(getDesiredPos());

  auto initialConfig = GMlib::Vector<float,4>(getNeckJointAngle(),
                                              getShoulderJointAngle(),
                                              getElbowJointAngle(),
                                              getWristJointAngle());

  std::vector<double> base_angles {20,130};
  std::vector<double> shoulder_angles {60,120};
  std::vector<double> elbow_angles {120,60};
  std::vector<double> wrist_angles {60,120};


  auto finalConfig = GMlib::Vector<float,4>(100,20,20,20);
  //    moveNeckMultiple(base_angles);
  //    moveShoulderMultiple(shoulder_angles);
  moveElbowMultiple(elbow_angles);
  //    moveWristMultiple(wrist_angles);

}

void Scenario::rotateTargetAngles()
{
  _ik.initializeParameters(base_neck,shoulder,arm,elbow,fore_arm,wrist,hand,pen,pen_tip);
  _ik.setTargetPosition(getDesiredPos());

  auto initialConfig = GMlib::Vector<float,4>(getNeckJointAngle(),
                                              getShoulderJointAngle(),
                                              getElbowJointAngle(),
                                              getWristJointAngle());
  double treshold = std::abs((_ik.getEndEffectorPosition() - _ik.getTargetPosition()).getLength());;
  double tempTresh = std::abs((_ik.getEndEffectorPosition() - _ik.getTargetPosition()).getLength());;
  int total = 10000;

  while (treshold > 0.1) {
    treshold = std::abs((_ik.getEndEffectorPosition() - _ik.getTargetPosition()).getLength());
    GMlib::Vector<float,4> updatedConfig = _ik.ComputeIK(initialConfig);

    rotateMatrices(_ik.getCurrentConfig());

    //    _ik.setNeuralInputTable();

    initialConfig = updatedConfig;
  }

  moveNeck(_ik.getCurrentConfig()[0]);
  moveShoulder(_ik.getCurrentConfig()[1]);
  moveElbow(_ik.getCurrentConfig()[2]);
  moveWrist(_ik.getCurrentConfig()[3]);


  _ik.setNeuralInputTable();
}

void Scenario::drawShape()
{
  _ik.initializeParameters(base_neck,shoulder,arm,elbow,fore_arm,wrist,hand,pen,pen_tip);

  auto initialConfig = GMlib::Vector<float,4>(getNeckJointAngle(),
                                              getShoulderJointAngle(),
                                              getElbowJointAngle(),
                                              getWristJointAngle());

  std::vector<GMlib::Point<float, 3>> positions = {GMlib::Point<float, 3>(0.0f, -6.722f, -0.750795f),
                                                   GMlib::Point<float, 3>(1,-6.722f, -0.750795f),
                                                   GMlib::Point<float, 3>(1,-5.722f, -0.750795f),
                                                   GMlib::Point<float, 3>(0,-5.722f, -0.750795f)};

  std::vector<double> neckAngles;
  std::vector<double> shoulderAngles;
  std::vector<double> elbowAngles;
  std::vector<double> wristAngles;

  for (auto position: positions) {
    initialConfig = moveToPoint(position,initialConfig);

    neckAngles.push_back(initialConfig[0]);
    shoulderAngles.push_back(initialConfig[1]);
    elbowAngles.push_back(initialConfig[2]);
    wristAngles.push_back(initialConfig[3]);
  }

  std::vector<double> base_angles {20,130};
  std::vector<double> shoulder_angles {60,120};
  std::vector<double> elbow_angles {120,60};
  std::vector<double> wrist_angles {90,98.02,100.1,90.9798};

  moveNeckMultiple    (neckAngles);
//  moveShoulderMultiple(shoulderAngles);
  moveElbowMultiple   (elbowAngles);
//  moveWristMultiple   (wristAngles);

  std::cout << "Neck angles are " << neckAngles <<std::endl;
  std::cout << "shoulder angles are " << shoulderAngles <<std::endl;
  std::cout << "elbow angles are " << elbowAngles <<std::endl;
  std::cout << "wrist angles are " << wristAngles <<std::endl;

  //  _ik.setNeuralInputTable();

}


GMlib::Vector<float,4> Scenario::moveToPoint(GMlib::Point<float, 3> desiredCoordinates,
                                             GMlib::Vector<float,4> initialConfig)
{
  _ik.setTargetPosition(desiredCoordinates);

  double treshold = std::abs((_ik.getEndEffectorPosition() - _ik.getTargetPosition()).getLength());;

  while (treshold > 0.1) {
    treshold = std::abs((_ik.getEndEffectorPosition() - _ik.getTargetPosition()).getLength());
    GMlib::Vector<float,4> updatedConfig = _ik.ComputeIK(initialConfig);

    rotateMatrices(_ik.getCurrentConfig());

    //    _ik.setNeuralInputTable();

    initialConfig = updatedConfig;
  }
  auto finalConfig = initialConfig;
  return finalConfig;
}

void Scenario::initializeParameters()
{
  _ik.setBaseMatrix(base_neck->getMatrix());
  _ik.setShoulderMatrix(shoulder->getMatrix());
  _ik.setElbowMatrix(elbow->getMatrix());
  _ik.setWristMatrix(wrist->getMatrix());
  _ik.setEndEffectorMatrix(hand->getMatrix());

  _ik.setBasePosition(base_neck->getGlobalPos());
  _ik.setShoulderPosition(shoulder->getGlobalPos());
  _ik.setElbowPosition(elbow->getGlobalPos());
  _ik.setWristPosition(wrist->getGlobalPos());
  _ik.setEndEffectorPosition(hand->getGlobalPos());
}

void Scenario::rotateMatrices(GMlib::Vector<float,4> newConfig)
{
  auto initialConfig = GMlib::Vector<float,4>(getNeckJointAngle(),
                                              getShoulderJointAngle(),
                                              getElbowJointAngle(),
                                              getWristJointAngle());


  auto deltaConfig = initialConfig - newConfig;

  GMlib::Vector<float,3> revolutionaryAxis = GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f);               //spatial orientation
  GMlib::Vector<float,3> prismaticAxis     = GMlib::Vector<float,3>(1.0f, 0.0f, 0.0f);

  auto revLIV = revolutionaryAxis.getLinIndVec();
  auto revU = revLIV ^ revolutionaryAxis;
  auto revV = revU ^ revolutionaryAxis;

  auto prisLIV = prismaticAxis.getLinIndVec();
  auto prisU = prisLIV ^ prismaticAxis;
  auto prisV = prisU ^ prismaticAxis;

  GMlib::HqMatrix<float,3> baseMatrix = base_neck->getMatrix();
  baseMatrix.rotate(int(deltaConfig[0]),revU,revV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
  _ik.setBaseMatrix(baseMatrix);

  GMlib::HqMatrix<float,3> shoulderMatrix = baseMatrix * shoulder->getMatrix();
  shoulderMatrix.rotate(int(deltaConfig[1]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
  _ik.setShoulderMatrix(shoulderMatrix);

  GMlib::HqMatrix<float,3> elbowMatrix = shoulderMatrix * arm->getMatrix()* elbow->getMatrix();
  elbowMatrix.rotate(int(deltaConfig[2]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
  _ik.setElbowMatrix(elbowMatrix);

  GMlib::HqMatrix<float,3> wristMatrix = elbowMatrix * fore_arm->getMatrix() * wrist->getMatrix();
  wristMatrix.rotate(int(deltaConfig[3]),prisU,prisV,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
  _ik.setWristMatrix(wristMatrix);

  auto endMatrix = wristMatrix * hand->getMatrix() * pen->getMatrix() * pen_tip->getMatrix();
  _ik.setEndEffectorMatrix(endMatrix);

  _ik.setBasePosition(GMlib::Point<float,3>(baseMatrix[0][3],baseMatrix[1][3], baseMatrix[2][3]));
  _ik.setShoulderPosition(GMlib::Point<float,3>(shoulderMatrix[0][3],shoulderMatrix[1][3], shoulderMatrix[2][3]));
  _ik.setElbowPosition(GMlib::Point<float,3>(elbowMatrix[0][3],elbowMatrix[1][3], elbowMatrix[2][3]));
  _ik.setWristPosition(GMlib::Point<float,3>(wristMatrix[0][3],wristMatrix[1][3], wristMatrix[2][3]));
  _ik.setEndEffectorPosition(GMlib::Point<float,3>(endMatrix[0][3],endMatrix[1][3], endMatrix[2][3]));

}

//getters
void Scenario::getJointGlobalPos()
{
  //  std::cout << "Final End Effector position: \n" << hand->getGlobalPos() <<std::endl;
}

int Scenario::getWristJointAngle()
{
  return wristJointAngle;
}

int Scenario::getElbowJointAngle()
{
  return elbowJointAngle;
}

int Scenario::getShoulderJointAngle()
{
  return shoulderJointAngle;
}

int Scenario::getNeckJointAngle()
{
  return neckJointAngle;
}

GMlib::Point<float, 3> Scenario::getEndEffectorPos()
{
  endEffectorPos = hand->getGlobalPos();
  return endEffectorPos;
}

GMlib::Point<float, 3> Scenario::getDesiredPos()
{
  return desiredPos;
}

//default
void Scenario::callDefferedGL() {
  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj[i]->replot();
}


