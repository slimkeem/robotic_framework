#ifndef ROBOTCYLINDER_H
#define ROBOTCYLINDER_H


// gmlib
#include <parametrics/surfaces/gmpcylinder.h>


class RobotCylinder : public GMlib::PCylinder<float> {
public:
  using PCylinder::PCylinder;

  ~RobotCylinder() override {

    if(m_test01)
      remove(test_01_cylinder.get());
  }

  void test01() {

    GMlib::Vector<float,3> d = evaluate(0.0f,0.0f,0,0)[0][0];
    test_01_cylinder = std::make_shared<RobotCylinder,float,float,float>(1.5f,0.5f,0.5f);

    test_01_cylinder->translate(d + d.getNormalized()*2.0f);
    test_01_cylinder->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f) );
    test_01_cylinder->toggleDefaultVisualizer();
    test_01_cylinder->sample(200,200,1,1);
    insert(test_01_cylinder.get());

    m_test01 = true;
  }


protected:
  void localSimulate(double dt) override {

//      static double t=0;
//      t += dt;

//      GMlib::Vector<float,3> vec(sin(t),cos(t),5*dt);
//      vec *= 0.05;
//      this->move(vec);
  }

private:
  bool m_test01 {false};
  std::shared_ptr<RobotCylinder> test_01_cylinder {nullptr};

}; // END class RobotCylinder



#endif // RobotCylinder_H
