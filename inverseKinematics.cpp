#include "inverseKinematics.hpp"
#include <cmath>
#include <iostream>

//MAIN FUNCTIONS
GMlib::Vector<float,4> InverseKinematics::ComputeIK(GMlib::Vector<float,4> initialConfig)
{
  currentConfig = initialConfig;
  treshold = std::abs((endEffectorPosition - targetPosition).getLength());
  ++count;
  auto deltaConfig = GetDeltaConfig();
  initialConfig += deltaConfig * simStep;

  currentConfig = initialConfig;
  return currentConfig;
}

GMlib::Vector<float, 4> InverseKinematics::GetDeltaConfig()
{
  auto Jt = GetJacobianTranspose();

  auto deltaPosition = targetPosition - endEffectorPosition;
  auto deltaConfig = Jt * deltaPosition;

  return deltaConfig;
}

GMlib::Matrix<float, 4, 3> InverseKinematics::GetJacobianTranspose()
{
  auto J_A = revolutionaryAxis ^ (endEffectorPosition - basePosition);
  auto J_B = prismaticAxis     ^ (endEffectorPosition - shoulderPosition);
  auto J_C = prismaticAxis     ^ (endEffectorPosition - elbowPosition);
  auto J_D = prismaticAxis     ^ (endEffectorPosition - wristPosition);

  GMlib::Matrix<float,3,4> jacobian;
  jacobian.setCol(J_A, 0);
  jacobian.setCol(J_B, 1);
  jacobian.setCol(J_C, 2);
  jacobian.setCol(J_D, 3);

  GMlib::Matrix<float,4,3> jacobianTranspose;
  jacobianTranspose.setRow(J_A, 0);
  jacobianTranspose.setRow(J_B, 1);
  jacobianTranspose.setRow(J_C, 2);
  jacobianTranspose.setRow(J_D, 3);

  auto deltaPosition = targetPosition - endEffectorPosition;
  auto deltaConfig = (jacobian * jacobianTranspose * deltaPosition);
  simStep = (deltaPosition* deltaConfig)/(deltaConfig*deltaConfig);
  return jacobianTranspose;
}


//PRIVATE GETTERS
GMlib::Point<float, 3> InverseKinematics::getBasePosition()
{
  return basePosition;
}

GMlib::Point<float, 3> InverseKinematics::getShoulderPosition()
{
  return shoulderPosition;
}

GMlib::Point<float, 3> InverseKinematics::getElbowPosition()
{
  return elbowPosition;
}

GMlib::Point<float, 3> InverseKinematics::getWristPosition()
{
  return wristPosition;
}

//PUBLIC GETTERS
GMlib::Point<float, 3> InverseKinematics::getEndEffectorPosition()
{
  return endEffectorPosition;
}

GMlib::Point<float, 3> InverseKinematics::getTargetPosition()
{
  return targetPosition;
}

GMlib::HqMatrix<float, 3> InverseKinematics::getBaseMatrix()
{
  return baseMatrix;
}

GMlib::HqMatrix<float, 3> InverseKinematics::getShoulderMatrix()
{
  return shoulderMatrix;
}

GMlib::HqMatrix<float, 3> InverseKinematics::getElbowMatrix()
{
  return elbowMatrix;
}

GMlib::HqMatrix<float, 3> InverseKinematics::getWristMatrix()
{
  return wristMatrix;
}

GMlib::HqMatrix<float, 3> InverseKinematics::getEndEffectorMatrix()
{
  return endEffectorMatrix;
}

GMlib::Vector<float, 4> InverseKinematics::getCurrentConfig()
{
  return currentConfig;
}

double InverseKinematics::getTreshold()
{
  return treshold;
}


//PUBLIC SETTERS
void InverseKinematics::initializeParameters(RotatingCylinder *base_neck, RotatingSphere *shoulder,
                                             RobotCylinder *arm, RotatingSphere *elbow,
                                             RobotCylinder *fore_arm, RotatingSphere *wrist,
                                             RobotCylinder *hand, RobotCylinder *pen,
                                             RotatingSphere *pen_tip)
{
  setBaseMatrix(base_neck->getMatrix());
  setShoulderMatrix(shoulder->getMatrix());
  setElbowMatrix(elbow->getMatrix());
  setWristMatrix(wrist->getMatrix());
  setEndEffectorMatrix(hand->getMatrix());

  localBaseMatrix = base_neck->getMatrix();
  localShoulderMatrix = shoulder->getMatrix();
  localArmMatrix = arm->getMatrix();
  localElbowMatrix = elbow->getMatrix();
  localForeArmMatrix = fore_arm->getMatrix();
  localWristMatrix = wrist->getMatrix();
  localHandMatrix = hand->getMatrix();
  localPenMatrix = pen->getMatrix();
  localTipMatrix = pen_tip->getMatrix();


  setBasePosition(base_neck->getGlobalPos());
  setShoulderPosition(shoulder->getGlobalPos());
  setElbowPosition(elbow->getGlobalPos());
  setWristPosition(wrist->getGlobalPos());
  setEndEffectorPosition(pen_tip->getGlobalPos());
}

void InverseKinematics::setNeuralInputTable()
{
  std::cout<<"initial Config: "    << currentConfig << std::endl;
  std::cout<<"Target Position: "   << targetPosition<< std::endl;
  std::cout<<"End Effector Pos: "  << endEffectorPosition<< std::endl;
  std::cout<<"Shoulder Position: " << shoulderPosition<< std::endl;
  std::cout<<"Elbow Position: "    << elbowPosition<< std::endl;
  std::cout<<"Wrist Position: "    << wristPosition<< std::endl;
  std::cout<<"Threshold : "        << treshold<< std::endl;
  std::cout<<"Epochs: "            << count << std::endl;
  std::cout<<"Sim Step: "          << simStep << std::endl;
}


void InverseKinematics::setTargetPosition(GMlib::Point<float, 3> &value)
{
  targetPosition = value;
  //  std::cout << "Target Position: \n" << targetPosition <<std::endl;
}

void InverseKinematics::setBaseMatrix(GMlib::HqMatrix<float, 3> value)
{
  baseMatrix = value;
}

void InverseKinematics::setShoulderMatrix(GMlib::HqMatrix<float,3> value)
{
  shoulderMatrix = value;
}

void InverseKinematics::setElbowMatrix(GMlib::HqMatrix<float,3> value)
{
  elbowMatrix = value;
}

void InverseKinematics::setWristMatrix(GMlib::HqMatrix<float,3> value)
{
  wristMatrix = value;
}

void InverseKinematics::setEndEffectorMatrix(GMlib::HqMatrix<float,3> value)
{
  endEffectorMatrix = value;
}

void InverseKinematics::setBasePosition(GMlib::Point<float,3> value)
{
  basePosition = value;
}

void InverseKinematics::setShoulderPosition(GMlib::Point<float,3> value)
{
  shoulderPosition = value;
}

void InverseKinematics::setElbowPosition(GMlib::Point<float,3> value)
{
  elbowPosition = value;
}

void InverseKinematics::setWristPosition(GMlib::Point<float,3> value)
{
  wristPosition = value;
}

void InverseKinematics::setEndEffectorPosition(GMlib::Point<float,3> value)
{
  endEffectorPosition = value;
}
