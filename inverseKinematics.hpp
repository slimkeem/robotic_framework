#ifndef INVERSEKINEMATICS_H
#define INVERSEKINEMATICS_H

//#include "scenario.h"
#include "forwardKinematics.hpp"

//blaze
#include <blaze/Math.h>

//stl
#include <vector>

// gmlib
#include <core/containers/gmarray.h>
#include <core/containers/gmdmatrix.h>
#include <scene/gmsceneobject.h>
#include "application/gmlibwrapper.h"
//#include <scene/utils/gmmaterial.h>

#include "robotcylinder.h"
#include "rotatingcylinder.h"
#include "rotatingsphere.h"


class InverseKinematics
{

  private:

    //PRIVATE VARIABLES
    double simStep = 0.01; // sim step; from 0.01 to 0.000000001

    //Joints and end effector Transformation Matrix
    GMlib::HqMatrix<float,3> baseMatrix;
    GMlib::HqMatrix<float,3> shoulderMatrix;
    GMlib::HqMatrix<float,3> elbowMatrix;
    GMlib::HqMatrix<float,3> wristMatrix;
    GMlib::HqMatrix<float,3> endEffectorMatrix;

    GMlib::HqMatrix<float,3> localBaseMatrix;
    GMlib::HqMatrix<float,3> localShoulderMatrix;
    GMlib::HqMatrix<float,3> localArmMatrix;
    GMlib::HqMatrix<float,3> localElbowMatrix;
    GMlib::HqMatrix<float,3> localForeArmMatrix;
    GMlib::HqMatrix<float,3> localWristMatrix;
    GMlib::HqMatrix<float,3> localHandMatrix;
    GMlib::HqMatrix<float,3> localPenMatrix;
    GMlib::HqMatrix<float,3> localTipMatrix;

    //Joints and end effector Orientation
    GMlib::Point<float,3> targetPosition;
    GMlib::Point<float,3> basePosition;
    GMlib::Point<float,3> shoulderPosition;
    GMlib::Point<float,3> elbowPosition;
    GMlib::Point<float,3> wristPosition;
    GMlib::Point<float,3> endEffectorPosition;

    //Rotational Axis
    GMlib::Vector<float,3> revolutionaryAxis = GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f);               //spatial orientation
    GMlib::Vector<float,3> prismaticAxis     = GMlib::Vector<float,3>(1.0f, 0.0f, 0.0f);               //spatial orientation


    GMlib::Vector<float,4> currentConfig;

    double  treshold;
    int     count;

    //PRIVATE GETTERS
    GMlib::Vector<float, 4> GetDeltaConfig();
    GMlib::Matrix<float,4,3> GetJacobianTranspose();

    GMlib::Point<float, 3> getBasePosition();
    GMlib::Point<float, 3> getShoulderPosition();
    GMlib::Point<float, 3> getElbowPosition();
    GMlib::Point<float, 3> getWristPosition();

    //PRIVATE SETTERS
    void rotateMatrices(GMlib::Vector<float, 4> newConfig);


  public:

    GMlib::Vector<float, 4> ComputeIK(GMlib::Vector<float, 4> initialConfig);

    //PUBLIC SETTERS
    void setTargetPosition(GMlib::Point<float, 3> &value);

    void setBaseMatrix(GMlib::HqMatrix<float, 3> value);
    void setShoulderMatrix(GMlib::HqMatrix<float,3> value);
    void setElbowMatrix(GMlib::HqMatrix<float,3> value);
    void setWristMatrix(GMlib::HqMatrix<float,3> value);
    void setEndEffectorMatrix(GMlib::HqMatrix<float,3> value);

    void setBasePosition(GMlib::Point<float, 3> value);
    void setShoulderPosition(GMlib::Point<float, 3> value);
    void setElbowPosition(GMlib::Point<float,3> value);
    void setWristPosition(GMlib::Point<float,3> value);
    void setEndEffectorPosition(GMlib::Point<float,3> value);

    //PUBLIC GETTERS
    GMlib::Point<float, 3> getEndEffectorPosition();
    GMlib::Point<float, 3> getTargetPosition();
    GMlib::HqMatrix<float, 3> getBaseMatrix();
    GMlib::HqMatrix<float, 3> getShoulderMatrix();
    GMlib::HqMatrix<float, 3> getElbowMatrix();
    GMlib::HqMatrix<float, 3> getWristMatrix();
    GMlib::HqMatrix<float, 3> getEndEffectorMatrix();
    GMlib::Vector<float, 4> getCurrentConfig();
    void initializeParameters(RotatingCylinder *base_neck, RotatingSphere *shoulder, RobotCylinder *arm,
                              RotatingSphere *elbow, RobotCylinder *fore_arm, RotatingSphere *wrist,
                              RobotCylinder *hand, RobotCylinder *pen, RotatingSphere *pen_tip);

    double getTreshold();
    void setNeuralInputTable();
};

#endif INVERSEKINEMATICS_H
